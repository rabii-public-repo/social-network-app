package com.social.network.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.social.network.dao.StatusDao;
import com.social.network.entities.Status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Transactional
public class StatusTest {

	@Autowired
	private StatusDao statusDao;
	private static final Logger log = LoggerFactory.getLogger(StatusTest.class);

	@Test
	public void testSave() {
		Status status = new Status("ceci un text de test");
		log.info("status est = {}", status);
		statusDao.save(status);
		
		assertNotNull("Non-null ID", status.getIdStatus());
		assertNotNull("Non-null Date", status.getDate());
		
		Status res = statusDao.findById(status.getIdStatus()).get();
		log.info("resultat est 	=	{}", res);
		assertEquals("ils doivent etre egaux", status, res);

	}
}
