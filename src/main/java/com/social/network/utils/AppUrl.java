package com.social.network.utils;

public final class AppUrl {

	public static final String HOME = "/";
	public static final String ABOUT = "/about";
	public static final String ADMIN = "/admin";
	public static final String ADD_STATUS = "/addstatus";
	public static final String VIEW_STATUS = "/viewstatus";

	private AppUrl() {
	}
}
