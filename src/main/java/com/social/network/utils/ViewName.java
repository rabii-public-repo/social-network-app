package com.social.network.utils;

public final class ViewName {

	public static final String HOME = "home";
	public static final String ABOUT = "about";
	public static final String ADMIN = "admin";
	public static final String ADD_STATUS = "addstatus";
	public static final String VIEW_STATUS = "viewstatus";

	private ViewName() {
	}
}
