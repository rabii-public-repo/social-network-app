package com.social.network.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.social.network.entities.Status;
import com.social.network.service.StatusService;
import com.social.network.utils.AppUrl;
import com.social.network.utils.ViewName;

@Controller
public class PageController {

	private final StatusService statusService;

	@Autowired
	public PageController(StatusService statusService) {
		this.statusService = statusService;
	}

	@RequestMapping(value = AppUrl.HOME, method = RequestMethod.GET)
	ModelAndView home(ModelAndView mav) {

		mav.setViewName(ViewName.HOME);

		Status status = this.statusService.getLatest();

		mav.getModel().put("status", status);
		
		return mav;
	}

	@RequestMapping(value = AppUrl.ABOUT, method = RequestMethod.GET)
	String about() {
		return ViewName.ABOUT;
	}

	@RequestMapping(value = AppUrl.ADMIN, method = RequestMethod.GET)
	String admin() {
		return ViewName.ADMIN;
	}

}
