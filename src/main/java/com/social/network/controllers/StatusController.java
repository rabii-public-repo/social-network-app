package com.social.network.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.social.network.entities.Status;
import com.social.network.service.StatusService;
import com.social.network.utils.AppUrl;
import com.social.network.utils.ViewName;

@Controller
public class StatusController {

	private static final Logger log = LoggerFactory.getLogger(StatusController.class);

	private final StatusService statusServ;

	@Autowired
	public StatusController(StatusService statusServ) {
		this.statusServ = statusServ;
	}

	@RequestMapping(value = AppUrl.ADD_STATUS, method = RequestMethod.GET)
	ModelAndView addStatus(ModelAndView mav) {

		mav.setViewName(ViewName.ADD_STATUS);

		Status status = new Status();

		mav.getModel().put("status", status);

		return mav;
	}

	@RequestMapping(value = AppUrl.ADD_STATUS, method = RequestMethod.POST)
	ModelAndView addStatus(ModelAndView mav, Status status) {

		mav.setViewName("redirect:"+AppUrl.VIEW_STATUS);

		this.statusServ.save(status);
		
		return mav;
	}

	@RequestMapping(value = AppUrl.VIEW_STATUS, method = RequestMethod.GET)
	ModelAndView viewStatus(ModelAndView mav,
			@RequestParam(name = "p", defaultValue = "1", required = false) int numberPage) {

		mav.setViewName(ViewName.VIEW_STATUS);

		Page<Status> page = statusServ.getPage(numberPage);

		mav.getModel().put("page", page);

		return mav;
	}
}
