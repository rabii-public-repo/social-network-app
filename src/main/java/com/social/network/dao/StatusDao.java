package com.social.network.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.social.network.entities.Status;

public interface StatusDao extends PagingAndSortingRepository<Status, Long> {
	Status findFirstByOrderByDateDesc();
}
