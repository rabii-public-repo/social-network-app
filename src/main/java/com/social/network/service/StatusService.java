package com.social.network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.social.network.dao.StatusDao;
import com.social.network.entities.Status;

@Service
public class StatusService {

	private static final int DEFAULT_SIZE = 3;

	private final StatusDao statusDao;

	@Autowired
	public StatusService(StatusDao statusDao) {
		this.statusDao = statusDao;
	}

	public Status save(Status status) {
		return this.statusDao.save(status);
	}

	public Page<Status> getPage(int pageNumber) {

		PageRequest request = PageRequest.of(pageNumber - 1, DEFAULT_SIZE, Sort.Direction.DESC, "date");

		return statusDao.findAll(request);
		// la méthode findAll remplace la requete suivante
		// SELECT * FROM status ORDER BY date 'Sort.Direction.DESC' LIMIT 'DEFAULT_SIZE'
	}

	public Status getLatest() {
		// TODO documentation sur SPRING DATA https://docs.spring.io/spring-data/jpa/docs/current/reference/html
			///#repositories.query-methods.query-creation
		return this.statusDao.findFirstByOrderByDateDesc();
	}
}
