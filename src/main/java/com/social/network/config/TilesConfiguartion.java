package com.social.network.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

@Configuration
public class TilesConfiguartion {
	private static final Logger log = LoggerFactory.getLogger(TilesConfiguartion.class);

	public TilesConfiguartion() {
		log.info("+++++     tiles configuration     ++++++");
	}

	// donner la definition de tiles
	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tilesConf = new TilesConfigurer();
		String[] definitions = { "/WEB-INF/tiles.xml" };
		tilesConf.setDefinitions(definitions);
		return tilesConf;
	}

	@Bean
	public UrlBasedViewResolver tilesViewResolver() {
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(TilesView.class);
		return viewResolver;
	}

}
