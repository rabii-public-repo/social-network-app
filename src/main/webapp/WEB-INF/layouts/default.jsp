<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>	


<!DOCTYPE html>
<html>
<head>

<%@ include file="/WEB-INF/includes/header.jsp"%>

<title><tiles:insertAttribute name="title" /></title>

</head>
<body>
	<%@ include file="/WEB-INF/includes/appUrl.jsp"%>
	<%@ include file="/WEB-INF/includes/navbar.jsp"%>
	<tiles:insertAttribute name="content" />

	<%@ include file="/WEB-INF/includes/script.jsp"%>

</body>
</html>