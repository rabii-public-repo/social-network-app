<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:url var="homeURL" value="/" />
<c:url var="aboutURL" value="/about" />
<c:url var="adminURL" value="/admin" />
<c:url var="addstatusURL" value="/addstatus" />
<c:url var="viewstatusURL" value="/viewstatus" />
