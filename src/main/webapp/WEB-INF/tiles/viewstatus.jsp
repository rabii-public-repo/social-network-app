<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags"%>


<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<div>
			<pagination:pagination page="${page}"
				viewstatusURL="${viewstatusURL}" size="10" />

		</div>

		<c:forEach var="status" items="${page.content}">

			<div class="panel panel-default">

				<div class="panel-heading">
					<div class="panel-title">
						Status update added on
						<fmt:formatDate pattern="EEEE d MMMM y 'at' H:mm:s"
							value="${status.date}" />
					</div>
				</div>

				<div class="panel-body">

					<c:out value="${status.text}" />

				</div>


			</div>

		</c:forEach>

	</div>
</div>
